#!/bin/bash

read -p "Digite um numero x: " n1
read -p "Digite uma operação (+, -, *, /, **, %): " op
read -p "Digite um numero z: " n2

resultado="$(($n1 $op $n2))"

echo "O resultado da operação de $n1 $op $n2 é ${resultado}"
